<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('halaman.index');
});

Route::get('/table', function () {
    return view('table.table');
});
Route::get('/data-table', function () {
    return view('table.data-table');
});

Route::get('/cast/create', 'castController@create');    

Route::post('/cast', 'castController@store');

Route::get('/cast', 'castController@index');

Route::get('/cast/{cast_id}', 'castController@show');

Route::get('/cast/{cast_id}/edit', 'castController@edit');

Route::put('/cast/{cast_id}', 'castController@update');

Route::delete('/cast/{cast_id}', 'castController@destroy');
