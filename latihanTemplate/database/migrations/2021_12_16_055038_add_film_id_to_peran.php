<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFilmIdToPeran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('peran', function (Blueprint $table) {
            $table->unsignedBigInteger('film_id');
            $table->foreign('film_id')->references('id')->on('film');
            $table->unsignedBigInteger('cast_id');
           $table->foreign('cast_id')->references('id')->on('cast');
        });

        /*Schema::table('film', function (Blueprint $table) {
            $table->unsignedBigInteger('genre_id');
            $table->foreign('genre_id')->references('id')->on('genre');
        });

        Schema::table('kritik', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('film_id');
            $table->foreign('user_id')->references('id')->on('user');
            $table->foreign('film_id')->references('id')->on('film');
        });

        Schema::table('profile', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('peran', function (Blueprint $table) {
           $table->dropForeign(['film_id']);
           $table->dropForeign(['cast_id']);
            $table->dropColumn('film_id');
            $table->dropColumn('cast_id');
        });

        /*Schema::table('film', function (Blueprint $table) {
            $table->dropForeign(['genre_id']);
            
            $table->dropColumn('genre_id');
            
        });

        Schema::table('kritik', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['film_id']);
            $table->dropColumn('user_id');
            $table->dropColumn('film_id');
            
        });

        Schema::table('profile', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            
            $table->dropColumn('user_id');
            
        });*/
    }
}
