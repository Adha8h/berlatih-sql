@extends('layout.master')

@section('judul')

Detail {{$cast->nama}}

@endsection

@section('subJudul')

<h2>Show Cast {{$cast->id}}</h2>
<a href="/cast" class="btn btn-primary">Home</a>
@endsection

@section('content') 
<h4>Nama: {{$cast->nama}}</h4>
<p>Umur: {{$cast->umur}}</p>
<p>Bio: {{$cast->bio}}</p>
@endsection