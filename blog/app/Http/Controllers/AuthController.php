<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
    	return view('register');
    }
    public function welcome(){
    	return view('welcome2');
    }
    public function welcome_post(Request $request){
        return view('welcome2', ['fname'=>$request['fname'], 'lname'=>$request['lname']]);
    }
}
